import download from "download";

const dirPath = "./files";

let arrUrl = [
  "https://drive.google.com/file/d/18iPB-OwfHMjYBdjHDgZyQiRO_RzbfFhZ",
  "https://dl2.worldofmods.ru/ru/installer/99895/1672158748/f768af3a555928a663b9398c9085bcd9ca81eb3f/",
  "https://dwweb.ru/__a-data/__all_for_scripts/__rar/pro_dw_koments_1_3.rar",
  "https://dl2.worldofmods.ru/ru/installer/99895/1672158748/f768af3a555928a663b9398c9085bcd9ca81eb3f/",
]

const fileLoader = (arrUrl) => {

  let pos = 0;

  const downloadFile = async (url) => {

    await download(url, dirPath)
    console.log(pos, " downloaded");
    pos++

    if (pos < arrUrl.length) {
      downloadFile(arrUrl[pos])
    }
  }
  // with .then
  // const downloadFile = (url) => {
  //   download(url, dirPath)
  //     .then(() => {
  //       console.log(pos, " downloaded");
  //       pos++
  //       if (pos < arrUrl.length) {
  //         downloadFile(arrUrl[pos])
  //       }
  //     })
  // }

  downloadFile(arrUrl[pos])
}

fileLoader(arrUrl)