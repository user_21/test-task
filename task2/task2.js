function largestNumbers(n) {

  let data = prompt("Введите текстовые данные");

  let arr = data.split(' ');

  let arrNumbers = arr.filter((item) => {
    return !(!item.match(/^[1-9]\d*$/) || isNaN(item));
  });

  let arrLargestNumbers = arrNumbers.sort((a, b) => {
    if (a.length == b.length) {
      return b > a ? 1 : -1;
    }
    return b.length - a.length;
  })

  alert(arrLargestNumbers.slice(0, n));

}

let n = prompt("Введите число n: ");

largestNumbers(n);