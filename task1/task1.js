const number = "1234567890";

const makePhoneNumber = (number) => {
  return number.replace(/^(\d{3})(\d{3})(\d{2})(\d{2})$/, '8 ($1) $2-$3-$4');
}

console.log(makePhoneNumber(number));