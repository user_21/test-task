const number = "1234567890000000";

const makeCardNumber = (number) => {
  return number.replace(/^(\d{4})(\d{4})(\d{4})(\d{4})$/, '$1 $2 $3 $4');
}

console.log(makeCardNumber(number));