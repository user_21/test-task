const loopify = (str) => {

  let strLength = str.length;

  position = 0;

  return () => {

    if (position == strLength) {
      position = 0;
    }

    return str[position++];
  }
}

const strloop = loopify("str");

console.log(strloop());
console.log(strloop());

console.log(strloop());
console.log(strloop());

console.log(strloop());
console.log(strloop());

console.log(strloop());
console.log(strloop());
