const areaCalculator = (type, params) => {

  if (type == "circle") {
    return circleCalculator(params);

  } else if (type == "triangle") {

    return triangleCalculator(params);
  }

}

const circleCalculator = (params) => {
  return (Math.PI * params * params);
}

const triangleCalculator = (params) => {

  [a, b, c] = params;

  let sum = params.reduce((acc, item) => acc + item, 0);

  let p = sum / 2;

  return (Math.sqrt(p * (p - a) * (p - b) * (p - c)));
}

console.log(areaCalculator("circle", 3));

console.log(areaCalculator("triangle", [2, 3, 4]));